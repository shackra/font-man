A simple global-minor-mode for changing the font's size temporally.

The name was inspired after Marvel's comic superhero [Ant-Man](https://en.wikipedia.org/wiki/Ant-Man) who can change its size at will, clever, right? ;)

I don't know what I'm doing, please report any bug you encounter.

## This is useful for...?

For live screen-casting, some sites like [Live Coding](https://www.livecoding.tv/) requires you to use large fonts so people watching you coding can read what you are writing.
