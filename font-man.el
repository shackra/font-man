;;; font-man.el --- Change font size temporally

;; Copyright (C) 2016 Jorge Araya Navarro

;; Author: Jorge Araya Navarro <elcorreo@deshackra.com>
;; Keywords: convenience
;; Version: 0.0.7
;; Homepage: http://bitbucket.org/shackra/font-man
;; Package-Requires: ((switch-buffer-functions "0.0.1"))

;; This file is not part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; font-man.el has everything needed to get this package rolling. BTW, font-man
;; is supposed to sound like Ant-man (you know, that Marvel superhero who can
;; change of size at will, you see, that's why the minor-mode lighter is an ant
;; :)

;;; Prayer:

;; Domine Iesu Christe, Fili Dei, miserere mei, peccatoris
;; Κύριε Ἰησοῦ Χριστέ, Υἱὲ τοῦ Θεοῦ, ἐλέησόν με τὸν ἁμαρτωλόν.
;; אדון ישוע משיח, בנו של אלוהים, רחם עליי, החוטא.
;; Nkosi Jesu Kristu, iNdodana kaNkulunkulu, ngihawukele mina, isoni.
;; Señor Jesucristo, Hijo de Dios, ten misericordia de mí, pecador.
;; Herr Jesus Christus, Sohn Gottes, hab Erbarmen mit mir Sünder.
;; Господи, Иисусе Христе, Сыне Божий, помилуй мя грешного/грешную.
;; Sinjoro Jesuo Kristo, Difilo, kompatu min pekulon.
;; Tuhan Yesus Kristus, Putera Allah, kasihanilah aku, seorang pendosa.
;; Bwana Yesu Kristo, Mwana wa Mungu, unihurumie mimi mtenda dhambi.
;; Doamne Iisuse Hristoase, Fiul lui Dumnezeu, miluiește-mă pe mine, păcătosul.
;; 主耶穌基督，上帝之子，憐憫我罪人。

;;; Code:

(require 'face-remap)
(require 'switch-buffer-functions)

(defgroup font-man nil "Change font size temporally on all buffers"
  :group 'convenience)

(defcustom font-man-scale-steps 2.3 "Default steps for scaling text"
  :type 'integer :group 'font-man :safe t)

(defun font-man--do-scale ()
  (if font-man-mode
      ;; When the minor global mode is on and the buffer wasn't scaled yet
      (when (and font-man-mode (and (<= text-scale-mode-amount 0) (not (= text-scale-mode-amount font-man-scale-steps))))
        (setf text-scale-mode-remapping
              (face-remap-add-relative 'default :height (expt text-scale-mode-step font-man-scale-steps)))
        (setf text-scale-mode-amount font-man-scale-steps))
    ;; When the minor global mode is off and the buffer was scaled
    (when (and (not font-man-mode) (>= text-scale-mode-amount 1))
      (face-remap-remove-relative text-scale-mode-remapping)
      (setf text-scale-mode-amount 0))))

;;;###autoload
(defun font-man-scale (&optional prev curr)
  "Scales all visible buffers by `FONT-MAN-SCALE-STEPS' steps"
  (let* ((frames (if (daemonp) (butlast (frame-list) 1) (frame-list)))
         (frame nil)
         (window nil)
         (windows nil))
    (dolist (frame frames)
      (setf frame (cadr frames))
      (setf windows (window-list frame))
      (dolist (window windows)
        (setf window (cadr windows))
        (with-current-buffer (window-buffer window)
          (font-man--do-scale))
        (force-window-update window)))))

;;;###autoload
(define-minor-mode font-man-mode "Change font size temporally"
  :lighter " 🐜"
  :group 'font-man
  :global t
  ;; This function works well here too
  (font-man-scale))

(add-hook 'switch-buffer-functions #'font-man-scale)
(add-hook 'window-configuration-change-hook #'font-man-scale)

(provide 'font-man)
;;; font-man.el ends here
